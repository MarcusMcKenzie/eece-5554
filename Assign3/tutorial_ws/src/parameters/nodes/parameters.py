#!/usr/bin/env python 
import roslib
import rospy



param = rospy.get_param('my_param')

if rospy.has_param('my_param'):
    
     print "Parameter value:", param
    
else:
    
    print "No parameter value"

    
