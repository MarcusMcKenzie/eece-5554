#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/marcus/tutorial_ws/devel:/opt/ros/kinetic"
export LD_LIBRARY_PATH="/home/marcus/tutorial_ws/devel/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu"
export PKG_CONFIG_PATH="/home/marcus/tutorial_ws/devel/lib/pkgconfig:/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig"
export PWD="/home/marcus/tutorial_ws/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/marcus/tutorial_ws/devel/share/common-lisp"