# CMake generated Testfile for 
# Source directory: /home/marcus/tutorial_ws/src
# Build directory: /home/marcus/tutorial_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(parameters)
subdirs(learning_tf)
