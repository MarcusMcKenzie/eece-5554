# how to use
# change the number of 'k' in line 40 from 14 to 1080 (for different images), and run. Whether if it is safe to go will show in console


import cv2
from IGPR import IGPR
import sys
import csv
import glob 
import os
from shutil import copyfile
from darknet import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as gImage

def load_csv(file_name):
    with open(file_name, "r") as f:
        reader = csv.reader(f)
        columns = [row for row in reader]

    columns = np.array(columns)
    m_x, n_x = columns.shape
    data_set = np.zeros((m_x, n_x))
    for i in range(m_x):
        for j in range(n_x):
            data_set[i][j] = float(columns[i][j])
    return data_set

def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h):
    label = class_id
    color = COLORS[0]
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


training_set = []
training_target = []

k = 314


COLORS = np.random.uniform(0, 255, size=(200, 3))

net = load_net("cfg/yolov3.cfg", "yolov3.weights", 0)
meta = load_meta("cfg/coco.data")
for file in glob.glob('*.mp4'):
	directory = '/home/marissa/Downloads/Classifier'
	os.chdir(directory)
	name = file[:-4]
	os.mkdir(name)
	vidcap = cv2.VideoCapture(file)
	success,image = vidcap.read()
	print(success)
	count = 0
	while success:
		cv2.imwrite( str(name) + "/frame%d.jpg" % count, image)     # save frame as JPEG file    
	 	success,image = vidcap.read() 
	 	img = gImage.imread('/home/marissa/Downloads/Classifier/' +str(name) + '/frame'+str(count)+'.jpg')
	 	r = detect(net, meta, '/home/marissa/Downloads/Classifier/'+str(name) + '/frame' +str(count)+'.jpg')

	    
		# np.save('training_set.npy', training_set)
		# np.save('training_target.npy',training_target)

		# np.savetxt('training_set.csv', training_set, delimiter = ',')
		# np.savetxt('training_target.csv', training_target, delimiter = ',')


		# GPR module
		training_set = load_csv('all_x.csv')
		training_target = load_csv('all_y.csv')
		data_len = training_target.shape[0]
		igpr = IGPR(training_set[0, :], training_target[0, :])
		for i in range(1, data_len):
		    print('iter', i)
		    igpr.learn(training_set[i, :], training_target[i, :])
		    print(" ")

		if_safe = True
		for i in range(len(r)):
		    
		    test_data = np.array([r[i][2][0], r[i][2][1], r[i][2][2], r[i][2][3]]).reshape((1, 4))
		    y_pred = igpr.predict(test_data)
		    print('box', i, [r[i][2][0], r[i][2][1], r[i][2][2], r[i][2][3]], 'y_pred', y_pred)
		    if y_pred > 0.5:
			y_pred = 1
		    else:
			y_pred = 0
		    if y_pred == 1:
			if_safe = False

		if if_safe:
		    print('free to go')
		else:
		    print('please wait')

		fig = plt.figure()
		ax = fig.add_subplot(111)
		# print('number of boxes', len(r))
		box_list = []
		for i in range(len(r)):
		    box_list.append([r[i][0], r[i][1], r[i][2][0], r[i][2][1], r[i][2][2], r[i][2][3]])
		for i in range(len(box_list)):
		    # print('box', i, box_list[i])
		    x_1 = int(box_list[i][2]-box_list[i][4]/2)
		    y_1 = int(box_list[i][3]-box_list[i][5]/2)
		    x_2 = int(box_list[i][2]+box_list[i][4]/2)
		    y_2 = int(box_list[i][3]+box_list[i][5]/2)
		    box_list[i][2] = x_1
		    box_list[i][3] = y_1
		    box_list[i][4] = x_2
		    box_list[i][5] = y_2
		    draw_bounding_box(img, str(i), box_list[i][1], box_list[i][2], box_list[i][3], box_list[i][4], box_list[i][5])
		    # training_set.append([r[i][2][0], r[i][2][1], r[i][2][2], r[i][2][3]])
 		    if if_safe:
		    	ax.text(3, 8, 'Safe', style='italic',bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})
		    else:
			ax.text(3, 8, 'Not Safe', style='italic',bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})
	       	plt.imshow(img)
	       	plt.savefig('/home/marissa/Downloads/Classifier/'+str(name) + '/' +str(count)+'safe.jpg')
	       	plt.close()
	       	count = count + 1
	directory = '/home/marissa/Downloads/Classifier/' + str(name)
	os.chdir(directory)  
	img_array = []; 
	for i in range(0,count-1):
		filename = '/home/marissa/Downloads/Classifier/' +str(name) + '/' + str(i) + 'safe.jpg'
		img = cv2.imread(filename)
		height, width, layers = img.shape
		size = (width,height)
		img_array.append(img)

	#out = cv2.VideoWriter('project.avi',cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
	out = cv2.VideoWriter('project.avi',cv2.VideoWriter_fourcc(*"mp4v"), 15, size)
	#out = cv2.VideoWriter('video.avi',-1,1,(width,height))

	 
	for i in range(len(img_array)):
	    out.write(img_array[i])
	cv2.destroyAllWindows()

	out.release()



