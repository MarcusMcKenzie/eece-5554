#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String

def publisher():
   pub = rospy.Publisher('topic', String, queue_size=10)
   rospy.init_node('publisher', anonymous=True)

   rate = rospy.Rate(10) # 10 Hz 

   while not rospy.is_shutdown():
	hello_str =  "i am publishing topic %s" % rospy.get_time()
	rospy.loginfo(hello_str)
	pub.publish(hello_str)
	rate.sleep()

if __name__ == '__main__':
   try:
	publisher()
   except rospy.ROSInterruptException:
	pass
